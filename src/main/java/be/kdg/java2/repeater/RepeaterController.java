package be.kdg.java2.repeater;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Optional;

@Controller
public class RepeaterController {
    @GetMapping("/repeater/{word}")
    public String repeatWord(@PathVariable("word") String word,
                             @RequestParam("repeat") Optional<Integer> repeat,
                             Model model){
        int theRepeat = Math.abs(repeat.orElse(1));
        model.addAttribute("repeatedword", word.repeat(theRepeat));
        return "repeater";
    }
}
